var gulp          = require('gulp');
var browserSync   = require('browser-sync').create();
var $             = require('gulp-load-plugins')();
var autoprefixer  = require('autoprefixer');
var autoprefixer  = require('autoprefixer');
var concat        = require('gulp-concat');
var rsync         = require('gulp-rsync');

var sassPaths = [
  'node_modules/foundation-sites/scss',
  'node_modules/motion-ui/src'
];

var jsComponents = [
  'node_modules/jquery/dist/jquery.js',
  'node_modules/what-input/dist/what-input.js',
  'node_modules/foundation-sites/dist/js/foundation.js'
]

var deployComponents = [
  'index.html',
  'js/*',
  'css/*',
]

function sass() {
  return gulp.src('scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.postcss([
      autoprefixer({ browsers: ['last 2 versions', 'ie >= 9'] })
    ]))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.stream());
}

function concat_js() {
  return gulp.src(jsComponents)
    .pipe(concat('base.js'))
    .pipe(gulp.dest('./js/'))
    .pipe(browserSync.stream());
}

function serve() {
  browserSync.init({
    server: "./"
  });

  gulp.watch("scss/*.scss", sass);
  gulp.watch("*.html").on('change', browserSync.reload);
  gulp.watch("js/app.js", concat_js);
}

function deploy() {
  return gulp.src(deployComponents)
    .pipe(rsync({
      root: '.',
      hostname: 'web547.webfaction.com',
      destination: '/home/nmnieto/webapps/demos_noenieto/chpt-konvertilo/'
    }));
}

gulp.task('concat_js', concat_js);
gulp.task('sass', sass);
gulp.task('serve', gulp.series('sass', serve));
gulp.task('default', gulp.series('sass', serve));
gulp.task('deploy', deploy);
