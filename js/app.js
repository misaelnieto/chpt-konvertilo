/*
    prettify_xml was taken from stackoverflow. MIT license? 
    https://stackoverflow.com/questions/376373/pretty-printing-xml-with-javascript/
*/
var prettify_xml = function(ugly_xml)
{
    var xmlDoc = new DOMParser().parseFromString(ugly_xml, 'application/xml');
    var xsltDoc = new DOMParser().parseFromString([
      // describes how we want to modify the XML - indent everything
        '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">',
        '  <xsl:strip-space elements="*"/>',
        '  <xsl:template match="para[content-style][not(text())]">', // change to just text() to strip space in text nodes
        '    <xsl:value-of select="normalize-space(.)"/>',
        '  </xsl:template>',
        '  <xsl:template match="node()|@*">',
        '    <xsl:copy><xsl:apply-templates select="node()|@*"/></xsl:copy>',
        '  </xsl:template>',
        '  <xsl:output indent="yes"/>',
        '</xsl:stylesheet>',
    ].join('\n'), 'application/xml');

    var xsltProcessor = new XSLTProcessor();    
    xsltProcessor.importStylesheet(xsltDoc);
    var resultDoc = xsltProcessor.transformToDocument(xmlDoc);
    var resultXml = new XMLSerializer().serializeToString(resultDoc);
    return resultXml;
};


function doConversion() {
    var src = document.getElementById("chpt");
    var dest = document.getElementById("mkv-xml");
    var lines = src.value.split('\n');
    var xmlDoc = document.implementation.createDocument(null, "Chapters");
    var xmlChapters = xmlDoc.getElementsByTagName("Chapters")[0];
    var ee = xmlChapters.appendChild(xmlDoc.createElement("EditionEntry"));
    var seed = new Date().getTime();

    // TODO: generate uuid instead of 1233456
    ee.appendChild(xmlDoc.createElement("EditionUID")).append((seed - 1).toString());
    
    for (var l = lines.length - 1; l >= 0; l-=2) {
        var rgx1 = /^CHAPTER(\d+)\s?=\s*(\d{2}):(\d{2}):(\d{2}.\d+)$/i; 
        var rgx2 = /^CHAPTER(\d+)NAME\s?=\s*(.*)$/i;

        if (rgx1.test(lines[l])) {
            let z = rgx1.exec(lines[l]);
            let ch = z[1], hh=z[2], mm=z[3], ss=z[4];
            let ca = ee.appendChild(xmlDoc.createElement("ChapterAtom"));
            let title = rgx2.exec(lines[l+1])[2];
            let cd = xmlDoc.createElement("ChapterDisplay");
            ca.appendChild(xmlDoc.createElement("ChapterTimeStart")).append(`${hh}:${mm}:${ss}`);
            ca.appendChild(cd);
            cd.appendChild(xmlDoc.createElement("ChapterString")).append(title);
            cd.appendChild(xmlDoc.createElement("ChapterLanguage")).append("spa");
            cd.appendChild(xmlDoc.createElement("ChapterCountry")).append("mx");
            ca.appendChild(xmlDoc.createElement("ChapterUID")).append(seed+l);
        }
    }
    var header = '<?xml version="1.0"?>\n<!-- <!DOCTYPE Chapters SYSTEM "matroskachapters.dtd"> -->\n';
    var content = new XMLSerializer().serializeToString(xmlDoc);
    dest.textContent = header + prettify_xml(content);
}


function message(msg_type, msg) {
    $("#messages").attr("class", `callout ${msg_type}`).text(msg);
    $("#messages").fadeIn(250);
    $("#messages").fadeOut(2500);
}


$(document).ready(function(){
    $(document).foundation();

    $("#chpt").on("input", function(evt){
        if (evt.target.textLength > 0) {
            $("#convert").prop('disabled', false);
        } else {
            $("#convert").prop('disabled', true);
        }
    });

    $("#convert").on("click", function(evt){
            doConversion();
        try {
            $("#copy").prop('disabled', false);
            $("#download").prop('disabled', false);
            message("success", "Conversion OK");
        }
        catch (e) {
            message("alert", "Something went wrong. Could not convert to XML.");
            console.log(e); // pass exception object to error handler
        }
    });

    $("#copy").on('click', function() {
        var mkvXml = document.getElementById("mkv-xml").select();
        if (document.execCommand("copy")) {
            console.log("Yeaah");
            message("success", "Copied to clipboard");
        } else {
            message("alert", "Your browser does not support copying to clipboard, you will have to do it manually.\n :(");
        }
    });

    $("#download").on("click", function (evt) {
        // Create file with results:
        var data = document.getElementById("mkv-xml").textContent;
        var properties = {type: 'application/xml', ContentDisposition: 'attachment'}; // Specify the file's mime-type.
        try {
          // Specify the filename using the File constructor, but ...
          file = new File([data], "chapters.xml", properties);
        } catch (e) {
          // ... fall back to the Blob constructor if that isn't supported.
          file = new Blob([data], properties);
        }
        var lnk = document.getElementById('link-helper');
        lnk.href = URL.createObjectURL(file);
        lnk.click();
    })
});
